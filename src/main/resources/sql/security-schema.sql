/* REMEMBER ME */
drop table if exists persistent_logins;
create table persistent_logins (
  username varchar(200) not null,
  series varchar(64) primary key,
  token varchar(64) not null,
  last_used timestamp not null,
  kb_session_id varchar(300) not null
);