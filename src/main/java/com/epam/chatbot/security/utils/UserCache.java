package com.epam.chatbot.security.utils;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.collections4.map.PassiveExpiringMap;

/**
 * Simple Cache for users.Users will be deleted after 15 minutes after entry.
 */

public class UserCache {

  private static UserCache instance = new UserCache();
  private static long EXPIRED_TIME_IN_MINUTES = 30;
  private static final Map<String, byte[]> cacheMap = new PassiveExpiringMap<>(
      EXPIRED_TIME_IN_MINUTES, TimeUnit.MINUTES);

  private UserCache() {
  }

  public static UserCache getInstance() {
    if (instance == null) {
     // synchronized (UserCache.class) {
       // if (instance == null) {
          instance = new UserCache();
        }
     // }
   // }
    return instance;
  }

  private class ObjectConverter {

    ObjectConverter() {
    }

    /**
     * Converts object to byte array
     */
    private byte[] convertToBytes(UserDetails object) throws IOException {
      try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
          ObjectOutput out = new ObjectOutputStream(bos)) {
        out.writeObject(object);
        return bos.toByteArray();
      }
    }

    /**
     * Converts byte array to object
     */
    private UserDetails convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
      try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
          ObjectInput in = new ObjectInputStream(bis)) {
        return (UserDetails) in.readObject();
      }
    }
  }

  /**
   * Converts user to byte array and puts in map
   */
  public void putObject(UserDetails userDetails) throws IOException {
    ObjectConverter objectConverter = new ObjectConverter();
    byte[] bytes = objectConverter.convertToBytes(userDetails);
    cacheMap.put(userDetails.getUser().getId(), bytes);
  }

  public UserDetails getObject(String id) throws IOException, ClassNotFoundException {
    ObjectConverter objectConverter = new ObjectConverter();
    UserDetails obj = objectConverter.convertFromBytes(cacheMap.get(id));
    return obj;
  }

  public void removeObj(String id) {
    cacheMap.remove(id);
  }

  public boolean containsUser(String id) {
    return cacheMap.containsKey(id);
  }

  public int getCacheSize() {
    return cacheMap.size();
  }
}
