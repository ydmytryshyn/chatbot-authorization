package com.epam.chatbot.security.utils;


import com.epam.chatbot.security.model.Activity;
import com.epam.chatbot.security.model.Reply;
import com.epam.chatbot.security.model.User;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javafx.util.Pair;


/**
 * Created by Victor on 05.07.2018.
 * Class that contains all user-related data during communication
 */
public class UserDetails implements Serializable {

  private Activity activity; // Activity which user sends to chat bot
  private User user; // Obvious user class ;)
  private Reply reply;

  private Map<String, Pair<String, String>> requestParams;

  public Map<String, Pair<String, String>> getRequestParams() {
    return requestParams;
  }

  public void setRequestParams(Map<String, Pair<String, String>> requestParams) {
    this.requestParams = requestParams;
  }

  public UserDetails(Activity activity, User user) {
    this.user = user;
    this.activity = activity;
    reply = new Reply();
    requestParams = new HashMap<>();

  }


  public Activity getActivity() {
    return activity;
  }

  public void setActivity(Activity activity) {
    this.activity = activity;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Reply getReply() {
    return reply;
  }

  public void setReply(Reply reply) {
    this.reply = reply;
  }
}
