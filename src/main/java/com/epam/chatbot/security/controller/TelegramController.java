package com.epam.chatbot.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The type Telegram controller.
 */
@Controller
@RequestMapping("/telegram")
public class TelegramController {

  /**
   * Gets login page.
   *
   * @return the login page
   */
  @GetMapping("/login")
  String getLoginPage() {
    return "login";
  }

}