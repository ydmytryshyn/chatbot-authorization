package com.epam.chatbot.security.controller;

import com.epam.chatbot.security.dto.DTOUtilMapper;
import com.epam.chatbot.security.dto.UserWithJsessionDTO;
import com.epam.chatbot.security.model.UserWithJsessionId;
import com.epam.chatbot.security.service.CustomRememberMeServices;
import com.epam.chatbot.security.service.ProfileInfoService;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * The type Authentication controller.
 */
@RestController
@RequestMapping("/api")
public class AuthenticationController {

  @Value("${origin-url:http://localhost:4200}")
  private String originURL;

  /**
   * The Dto util mapper.
   */
  @Autowired
  DTOUtilMapper dtoUtilMapper;

  /**
   * The Profile info service.
   */
  @Autowired
  ProfileInfoService profileInfoService;

  /**
   * The Custom remember me services.
   */
  @Autowired
  CustomRememberMeServices customRememberMeServices;

  /**
   * Log in success response entity.
   *
   * @return the response entity
   */
  @PostMapping(value = "/restricted-login-success")
  public ResponseEntity<?> logInSuccess() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    UserWithJsessionId userWithJsessionId = (UserWithJsessionId) authentication.getPrincipal();
    Map<String, String> response = new HashMap<>();
    response.put("status", "logged in");
    response.put("email", userWithJsessionId.getUsername());
    return new ResponseEntity<>(response, HttpStatus.OK);
  }


  /**
   * Log in fail response entity.
   *
   * @return the response entity
   */
  @PostMapping(value = "/restricted-login-fail")
  public ResponseEntity<?> logInFail() {
    Map<String, Object> unauthorizedResponse = new HashMap<>();
    unauthorizedResponse.put("timestamp", new Date());
    unauthorizedResponse.put("status", HttpStatus.UNAUTHORIZED.value());
    unauthorizedResponse.put("error", "Unauthorized");
    unauthorizedResponse.put("message", "Full authentication is required to access this resource");

    return new ResponseEntity<>(unauthorizedResponse, HttpStatus.UNAUTHORIZED);
  }

  /**
   * Log out response entity.
   *
   * @param request the request
   * @param response the response
   * @return the response entity
   */
  @GetMapping(value = "/logout")
  public ResponseEntity<?> logOut(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      new SecurityContextLogoutHandler().logout(request, response, auth);
      customRememberMeServices.logout(request, response, auth);
    }
    Map<String, String> resultResponse = new HashMap<String, String>();
    resultResponse.put("status", "logged out");
    return new ResponseEntity<>(resultResponse, HttpStatus.OK);
  }

  /**
   * Gets user info.
   *
   * @param authentication the authentication
   * @return the user info
   */
  @GetMapping(value = "/getUserInfo")
  public ResponseEntity<?> getUserInfo(Authentication authentication) {
    String restUri = "https://kb.epam.com/users/viewmyprofile.action";

    UserWithJsessionId userWithJsessionId = (UserWithJsessionId) authentication.getPrincipal();

    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers = new HttpHeaders();
    headers.add("Cookie", "" + userWithJsessionId.getKbJsessionId());
    HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(
        null, headers);
    ResponseEntity<String> response = restTemplate
        .exchange(restUri, HttpMethod.GET, entity, String.class);
    String htmlText = response.toString();
    Document document = Jsoup.parse(htmlText);
    UserWithJsessionDTO userWithJsessionDTO = dtoUtilMapper.convertToDto(profileInfoService.
        getUserWithProfileInfo(userWithJsessionId, document));
    return new ResponseEntity<UserWithJsessionDTO>(userWithJsessionDTO, HttpStatus.OK);
  }

  /**
   * Gte user details response entity.
   *
   * @param authentication the authentication
   * @return the response entity
   */
/*
  * Required for angular login logic.
   */
  @GetMapping(value = "/userDetails")
  public ResponseEntity<?> gteUserDetails(Authentication authentication) {
    // UserWithJsessionId user = (UserWithJsessionId) authentication.getPrincipal();
    return new ResponseEntity<Authentication>(authentication, HttpStatus.OK);
  }

  /**
   * Gets ping.
   *
   * @return the ping
   */
  @GetMapping("/ping")
  public ResponseEntity<?> getPing() {
    return new ResponseEntity<String>("OK", HttpStatus.OK);
  }

  /**
   * Gets kb jsession id.
   *
   * @param authentication the authentication
   * @return the kb jsession id
   */
  @GetMapping("/getKbJsessionId")
  public ResponseEntity<?> getKbJsessionId(Authentication authentication) {
    UserWithJsessionId userWithJsessionId = (UserWithJsessionId) authentication.getPrincipal();
    Map<String, String> response = new HashMap<String, String>();
    response.put("upsaToken", userWithJsessionId.getKbJsessionId());
    return new ResponseEntity<Object>(response, HttpStatus.OK);
  }


}
