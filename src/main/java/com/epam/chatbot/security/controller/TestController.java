package com.epam.chatbot.security.controller;

import com.epam.chatbot.security.model.Activity;
import com.epam.chatbot.security.model.User;
import com.epam.chatbot.security.model.UserWithJsessionId;
import com.epam.chatbot.security.service.BotService;
import com.epam.chatbot.security.service.JsonParserService;
import com.epam.chatbot.security.utils.IntentTypes;
import com.epam.chatbot.security.utils.UserCache;
import com.epam.chatbot.security.utils.UserDetails;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * The type Test controller.
 */
@RestController
@RequestMapping("/api")
public class TestController {


  /**
   * The Bot service.
   */
  @Autowired
  BotService botService;
  /**
   * The Json parser service.
   */
  @Autowired
  JsonParserService jsonParserService;


  /**
   * Test upsa request response entity.
   *
   * @param authentication the authentication
   * @return the response entity
   */
  @GetMapping("/testKb")
  public ResponseEntity<?> testUpsaRequest(Authentication authentication) {
    String restUri = "https://kb.epam.com/rest/experimental/search?cql=siteSearch+~+%22java%22+and+contributor+%3D+%22Vladimir_Kadrov%22&queryString=java";

    UserWithJsessionId userWithJsessionId = (UserWithJsessionId) authentication.getPrincipal();
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.add("Cookie", "" + userWithJsessionId.getKbJsessionId());
    headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
    HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(
        null, headers);
    ResponseEntity<String> response = restTemplate
        .exchange(restUri, HttpMethod.GET, entity, String.class);

    return response;
  }

  /**
   * Test message activity.
   *
   * @param activity the activity
   * @param authentication the authentication
   * @return the activity
   * @throws IOException the io exception
   * @throws ClassNotFoundException the class not found exception
   */
  @PostMapping("/messages")
  public Activity testMessage(@RequestBody Activity activity,
      Authentication authentication)
      throws IOException, ClassNotFoundException {

    UserWithJsessionId userWithJsessionId = (UserWithJsessionId) authentication.getPrincipal();

    UserDetails userDetails;

    UserCache userCache = UserCache.getInstance();
    if (userCache.containsUser(userWithJsessionId.getKbJsessionId())) {
      userDetails = userCache.getObject(userWithJsessionId.getKbJsessionId());
    } else {
      User user = new User();
      user.setId(userWithJsessionId.getKbJsessionId());
      user.setIntentType(IntentTypes.HELLO);
      userDetails = new UserDetails(activity, user);
    }
    userDetails.setActivity(activity);
    userCache.putObject(userDetails);

    return botService.interactWithBot(userDetails);
  }
}
