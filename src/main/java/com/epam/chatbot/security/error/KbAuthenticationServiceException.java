package com.epam.chatbot.security.error;

/**
 * The type Kb authentication service exception.
 */
public class KbAuthenticationServiceException extends Exception {

  /**
   * Instantiates a new Kb authentication service exception.
   *
   * @param description the description
   */
  public KbAuthenticationServiceException(String description) {
    super(description);
  }
}
