package com.epam.chatbot.security.model;


import com.epam.chatbot.security.utils.IntentTypes;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * Created by Victor on 05.07.2018.
 */
@Entity
public class Reply implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String reply;


  @Enumerated
  private IntentTypes intentType;

  @Transient
  private boolean confirmed;

  /**
   * Instantiates a new Reply.
   */
  public Reply() {
    confirmed = false;
  }

  /**
   * Instantiates a new Reply.
   *
   * @param reply the reply
   * @param intentType the intent type
   */
  public Reply(String reply, IntentTypes intentType) {
    this.reply = reply;
    this.intentType = intentType;
  }

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Sets id.
   *
   * @param id the id
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Gets reply.
   *
   * @return the reply
   */
  public String getReply() {
    return reply;
  }

  /**
   * Sets reply.
   *
   * @param reply the reply
   */
  public void setReply(String reply) {
    this.reply = reply;
  }

  /**
   * Gets intent type.
   *
   * @return the intent type
   */
  public IntentTypes getIntentType() {
    return intentType;
  }

  /**
   * Sets intent type.
   *
   * @param intentType the intent type
   */
  public void setIntentType(IntentTypes intentType) {
    this.intentType = intentType;
  }

  /**
   * Is confirmed boolean.
   *
   * @return the boolean
   */
  public boolean isConfirmed() {
    return confirmed;
  }

  /**
   * Sets confirmed.
   *
   * @param confirmed the confirmed
   */
  public void setConfirmed(boolean confirmed) {
    this.confirmed = confirmed;
  }

}
