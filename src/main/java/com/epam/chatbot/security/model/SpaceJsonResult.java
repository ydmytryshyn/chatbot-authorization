package com.epam.chatbot.security.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 * The type Space json result.
 */
@Component
public class SpaceJsonResult {

  @JsonProperty("results")
  private List<Space> spaces;

  /**
   * Gets spaces.
   *
   * @return the spaces
   */
  public List<Space> getSpaces() {
    return spaces;
  }

  /**
   * Sets spaces.
   *
   * @param spaces the spaces
   */
  public void setSpaces(List<Space> spaces) {
    this.spaces = spaces;
  }

  @Override
  public String toString() {
    return "SpaceJsonResult{" +
        "spaces=" + spaces +
        '}';
  }
}
