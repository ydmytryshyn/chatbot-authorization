package com.epam.chatbot.security.model;


import java.util.List;

/**
 * The type Result of request.
 */
public class ResultOfRequest {

  private List<Result> results;

  /**
   * Gets results.
   *
   * @return the results
   */
  public List<Result> getResults() {
    return results;
  }

  @Override
  public String toString() {
    return "ResultOfRequest{" +
        "posts=" + results +
        '}';
  }
}
