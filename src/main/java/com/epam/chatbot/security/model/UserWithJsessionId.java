package com.epam.chatbot.security.model;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * The type User with jsession id.
 */
public class UserWithJsessionId extends User {

  private static final long serialVersionUID = 1285499039156736247L;

  private String kbJsessionId;
  private String fullName;
  private String email;
  private String phone;
  private String im;
  private String webSite;
  private String position;
  private String department;
  private String location;


  /**
   * Instantiates a new User with jsession id.
   *
   * @param username the username
   * @param password the password
   * @param authorities the authorities
   * @param kbJsessionId the kb jsession id
   */
  public UserWithJsessionId(String username, String password,
      Collection<? extends GrantedAuthority> authorities, String kbJsessionId) {
    super(username, password, authorities);
    this.kbJsessionId = kbJsessionId;
  }

  /**
   * Gets kb jsession id.
   *
   * @return the kb jsession id
   */
  public String getKbJsessionId() {
    return kbJsessionId;
  }

  /**
   * Gets full name.
   *
   * @return the full name
   */
  public String getFullName() {
    return fullName;
  }

  /**
   * Sets full name.
   *
   * @param fullName the full name
   */
  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  /**
   * Gets email.
   *
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * Sets email.
   *
   * @param email the email
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * Gets phone.
   *
   * @return the phone
   */
  public String getPhone() {
    return phone;
  }

  /**
   * Sets phone.
   *
   * @param phone the phone
   */
  public void setPhone(String phone) {
    this.phone = phone;
  }

  /**
   * Gets im.
   *
   * @return the im
   */
  public String getIm() {
    return im;
  }

  /**
   * Sets im.
   *
   * @param im the im
   */
  public void setIm(String im) {
    this.im = im;
  }

  /**
   * Gets web site.
   *
   * @return the web site
   */
  public String getWebSite() {
    return webSite;
  }

  /**
   * Sets web site.
   *
   * @param webSite the web site
   */
  public void setWebSite(String webSite) {
    this.webSite = webSite;
  }

  /**
   * Gets position.
   *
   * @return the position
   */
  public String getPosition() {
    return position;
  }

  /**
   * Sets position.
   *
   * @param position the position
   */
  public void setPosition(String position) {
    this.position = position;
  }

  /**
   * Gets department.
   *
   * @return the department
   */
  public String getDepartment() {
    return department;
  }

  /**
   * Sets department.
   *
   * @param department the department
   */
  public void setDepartment(String department) {
    this.department = department;
  }

  /**
   * Gets location.
   *
   * @return the location
   */
  public String getLocation() {
    return location;
  }

  /**
   * Sets location.
   *
   * @param location the location
   */
  public void setLocation(String location) {
    this.location = location;
  }
}
