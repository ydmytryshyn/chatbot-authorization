package com.epam.chatbot.security.model;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by Victor on 22.07.2018.
 */
public class Activity implements Serializable {

  private Map<String, String> linkArticle;
  private String text;
  private String time;
  private boolean isBot;
  private String type;

  /**
   * Gets text.
   *
   * @return the text
   */
  public String getText() {
    return text;
  }

  /**
   * Sets text.
   *
   * @param text the text
   */
  public void setText(String text) {
    this.text = text;
  }

  /**
   * Gets time.
   *
   * @return the time
   */
  public String getTime() {
    return time;
  }

  /**
   * Sets time.
   *
   * @param time the time
   */
  public void setTime(String time) {
    this.time = time;
  }

  /**
   * Is bot boolean.
   *
   * @return the boolean
   */
  public boolean isBot() {
    return isBot;
  }

  /**
   * Sets bot.
   *
   * @param bot the bot
   */
  public void setBot(boolean bot) {
    isBot = bot;
  }

  /**
   * Gets type.
   *
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * Sets type.
   *
   * @param type the type
   */
  public void setType(String type) {
    this.type = type;
  }
}
