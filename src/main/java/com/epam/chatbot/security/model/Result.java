package com.epam.chatbot.security.model;


/**
 * The type Result.
 */
public class Result {

  private Content content;

  /**
   * Gets content.
   *
   * @return the content
   */
  public Content getContent() {
    return content;
  }

  @Override
  public String toString() {
    return "Result{" +
        "content=" + content +
        '}';
  }
}
