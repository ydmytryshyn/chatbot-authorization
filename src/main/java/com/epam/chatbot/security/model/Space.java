package com.epam.chatbot.security.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The type Space.
 */
public class Space {

  @JsonProperty("key")
  private String key;
  @JsonProperty("name")
  private String name;

  /**
   * Instantiates a new Space.
   *
   * @param key the key
   * @param name the name
   */
  public Space(String key, String name) {
    this.key = key;
    this.name = name;
  }

  /**
   * Gets key.
   *
   * @return the key
   */
  public String getKey() {
    return key;
  }

  /**
   * Sets key.
   *
   * @param key the key
   */
  public void setKey(String key) {
    this.key = key;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets name.
   *
   * @param name the name
   */
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "Space{" +
        "key='" + key + '\'' +
        ", name='" + name + '\'' +
        '}';
  }
}
