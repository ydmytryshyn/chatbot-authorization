package com.epam.chatbot.security.model;

import com.epam.chatbot.security.utils.IntentTypes;
import java.io.Serializable;


/**
 * The type User.
 */
public class User implements Serializable {

  private String id;

  private IntentTypes intentType;

  /**
   * Instantiates a new User.
   */
  public User() {
  }


  /**
   * Gets id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Sets id.
   *
   * @param id the id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Gets intent type.
   *
   * @return the intent type
   */
  public IntentTypes getIntentType() {
    return intentType;
  }

  /**
   * Sets intent type.
   *
   * @param intentType the intent type
   */
  public void setIntentType(IntentTypes intentType) {
    this.intentType = intentType;
  }
}
