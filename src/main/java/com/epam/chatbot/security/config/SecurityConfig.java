package com.epam.chatbot.security.config;

import com.epam.chatbot.security.service.CustomRememberMeServices;
import com.epam.chatbot.security.service.util.CustomJdbcTokenRepositoryImpl;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;


/**
 * The type Security config.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Value("${remember-me-key:secret-key}")
  private String rememberMeKey;

  @Value("${remember-me-cookie-name:chat-bot-remember-me}")
  private String rememberMeCookieName;

  @Autowired
  private AuthenticationProvider authenticationProvider;

  @Autowired
  private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

  @Autowired
  private DataSource dataSource;

  @Override
  protected void configure(AuthenticationManagerBuilder auth) {
    auth.authenticationProvider(authenticationProvider);
    auth.authenticationProvider(new RememberMeAuthenticationProvider(rememberMeKey));
  }

  @Override
  protected void configure(HttpSecurity security) throws Exception {
    /*
    security.csrf().disable().authorizeRequests().antMatchers("/").permitAll();
    */
    security.csrf().disable()
        .exceptionHandling()
        .authenticationEntryPoint(restAuthenticationEntryPoint)
        .and()
        .authorizeRequests()
        .antMatchers("/api/login", "/telegram/login").permitAll()
        //.antMatchers("/api/messages").permitAll()
        .antMatchers("/api/restricted-*").denyAll()
        .anyRequest().authenticated()
        .and()
        .formLogin()
        .failureHandler(new SimpleUrlAuthenticationFailureHandler())
        .loginProcessingUrl("/api/login")
        .successForwardUrl("/api/restricted-login-success")
        .and()
        .rememberMe()
        .rememberMeServices(customRememberMeServices())
        .and()
        .logout().disable();
  }

  /**
   * Custom remember me services custom remember me services.
   *
   * @return the custom remember me services
   */
  @Bean
  public CustomRememberMeServices customRememberMeServices() {
    CustomJdbcTokenRepositoryImpl jdbcTokenRepositoryImpl = new CustomJdbcTokenRepositoryImpl();
    jdbcTokenRepositoryImpl.setDataSource(dataSource);
    CustomRememberMeServices customRememberMeServices = new CustomRememberMeServices(rememberMeKey,
        jdbcTokenRepositoryImpl);
    customRememberMeServices.setCookieName(rememberMeCookieName);
    customRememberMeServices.setParameter("remember-me");
    customRememberMeServices.setTokenValiditySeconds(86400); // 24 hours
    return customRememberMeServices;
  }

  /**
   * Http session event publisher http session event publisher.
   *
   * @return the http session event publisher
   */
/*
   * To invalidate the active session of the user and authenticate the user again with a new session
   */
  @Bean
  public HttpSessionEventPublisher httpSessionEventPublisher() {
    return new HttpSessionEventPublisher();
  }
}
