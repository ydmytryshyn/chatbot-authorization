package com.epam.chatbot.security.repository;

import com.epam.chatbot.security.model.Reply;
import com.epam.chatbot.security.utils.IntentTypes;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Victor on 05.07.2018.
 */
public interface ReplyRepository extends JpaRepository<Reply, Long> {

  /**
   * Find all by intent type list.
   *
   * @param intentType the intent type
   * @return the list
   */
  List<Reply> findAllByIntentType(IntentTypes intentType);
}
