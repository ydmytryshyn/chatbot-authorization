package com.epam.chatbot.security.service;


import com.epam.chatbot.security.utils.UserDetails;

/**
 * Created by Victor on 04.07.2018.
 */
public interface TextProcessor {

  /**
   * Decide.
   *
   * @param userDetails the user details
   */
  void decide(UserDetails userDetails);


}
