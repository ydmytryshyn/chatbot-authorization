package com.epam.chatbot.security.service;

import com.epam.chatbot.security.error.KbAuthenticationServiceException;
import java.io.IOException;
import java.net.HttpURLConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * The type Kb authentication service.
 */
@Service
@Scope("prototype")
public class KbAuthenticationService {

  /**
   * The constant logger.
   */
  public static final Logger logger = LoggerFactory.getLogger(KbAuthenticationService.class);

  @Value("${kb-authorization-url:https://login.epam.com/adfs/oauth2/authorize?resource=https://kb.epam.com&response_type=code&redirect_uri=https://kb.epam.com/sso/callback&client_id=kb}")
  private String authorizationURL;

  /**
   * Authenticate string.
   *
   * @param userName the user name
   * @param password the password
   * @return the string
   * @throws KbAuthenticationServiceException the kb authentication service exception
   */
  public String authenticate(String userName, String password)
      throws KbAuthenticationServiceException {

    String kbJsessionId = "";

    try {
      RestTemplate restTemplate = new RestTemplate(new SimpleClientHttpRequestFactory() {
        protected void prepareConnection(HttpURLConnection connection, String httpMethod)
            throws IOException {
          super.prepareConnection(connection, httpMethod);
          connection.setInstanceFollowRedirects(false);
        }
      });

      // First request
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

      MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
      parameters.add("UserName", userName);
      parameters.add("Password", password);
      parameters.add("AuthMethod", "FormsAuthentication");

      HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(
          parameters, headers);

      ResponseEntity<String> response = restTemplate
          .exchange(authorizationURL, HttpMethod.POST, entity, String.class);

      headers = response.getHeaders();
      String cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
      logger.debug("First request status code: {}", response.getStatusCode());
      logger.debug("First request cookies: {}", cookie);

      // Second request
      headers = new HttpHeaders();
      headers.add("Cookie", cookie);
      entity = new HttpEntity<MultiValueMap<String, String>>(null, headers);

      response = restTemplate.exchange(authorizationURL, HttpMethod.POST, entity, String.class);

      headers = response.getHeaders();
      cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
      logger.debug("Second request status code: {}", response.getStatusCode());
      logger.debug("Second request cookies: {}", cookie);
      String location = headers.getFirst("Location");
      logger.debug("Second request location: {}", location);

      // Third request
      String locationUrl = headers.getFirst("Location");

      response = restTemplate
          .exchange(locationUrl, HttpMethod.GET, null, String.class);

      headers = response.getHeaders();
      cookie = headers.getFirst(HttpHeaders.SET_COOKIE);
      logger.debug("Third request status code: {}", response.getStatusCode());
      logger.debug("Third request cookies: {}", cookie);

      kbJsessionId = cookie;

    } catch (Exception ex) {
      logger.warn("User is not authenticated: {}", ex.toString());
      throw new KbAuthenticationServiceException("KbAuthenticationServiceException");
    }

    if (kbJsessionId.isEmpty()) {
      logger.warn("User is not authenticated: kbJsessionId is empty");
      throw new KbAuthenticationServiceException("KbAuthenticationServiceException");
    }

    return kbJsessionId;
  }

}
