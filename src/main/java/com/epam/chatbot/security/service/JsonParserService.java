package com.epam.chatbot.security.service;

import com.epam.chatbot.security.dto.Example;
import com.epam.chatbot.security.dto.Result;
import com.epam.chatbot.security.model.Space;
import com.epam.chatbot.security.model.SpaceJsonResult;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * The type Json parser service.
 */
@Service
public class JsonParserService {

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  Example example;

  @Autowired
  SpaceJsonResult spaceJsonResult;


  /**
   * Parse json from kb response list.
   *
   * @param responseJson the response json
   * @return the list
   * @throws IOException the io exception
   */
  public List<Result> parseJsonFromKbResponse(HttpResponse<JsonNode> responseJson)
      throws IOException {
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    example = objectMapper.readValue(responseJson.getBody().toString(), Example.class);
    List<Result> results = example.getResults();
    return results;
  }

  /**
   * Parse space json list.
   *
   * @param responseJson the response json
   * @return the list
   * @throws IOException the io exception
   */
  public List<Space> parseSpaceJson(ResponseEntity<String> responseJson) throws IOException {
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    spaceJsonResult = objectMapper.readValue(responseJson.getBody(), SpaceJsonResult.class);
    List<Space> spaces = spaceJsonResult.getSpaces();
    return spaces;
  }
}
