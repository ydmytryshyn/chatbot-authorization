package com.epam.chatbot.security.service;

/**
 * Created by Victor on 04.07.2018.
 */


import com.epam.chatbot.security.model.Activity;
import java.util.Date;

/**
 * Factory class to produce various activities
 */
public final class ActivityFactory {

  /**
   * Creates a new instance of {@link ActivityFactory}
   */
  private ActivityFactory() {

  }

  /**
   * Creates a activity for a reply
   *
   * We form new activity, which bot will send back to a user
   *
   * @param text Text for the reply
   * @return Returns Activity which must be send to user
   */
  public static Activity createReply(String text) {
    Activity reply = new Activity();

    reply.setText(text);
    reply.setTime(new Date(System.currentTimeMillis()).toString());
    return reply;
  }


}
