package com.epam.chatbot.security.service;

import com.epam.chatbot.security.model.UserWithJsessionId;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

/**
 * The type Profile info service.
 */
@Service
public class ProfileInfoService {

  /**
   * Gets user with profile info.
   *
   * @param userWithJsessionId the user with jsession id
   * @param document the document
   * @return the user with profile info
   */
  public UserWithJsessionId getUserWithProfileInfo(UserWithJsessionId userWithJsessionId,
      Document document) {
    userWithJsessionId.setFullName(document.getElementById("fullName").ownText());
    userWithJsessionId.setEmail(document.getElementById("email").ownText());
    userWithJsessionId.setPhone(document.getElementById("userparam-phone").ownText());
    userWithJsessionId.setWebSite(document.getElementById("userparam-website").ownText());
    userWithJsessionId.setDepartment(document.getElementById("userparam-department").ownText());
    userWithJsessionId.setLocation(document.getElementById("userparam-location").ownText());
    userWithJsessionId.setIm(document.getElementById("userparam-im").ownText());
    userWithJsessionId.setPosition(document.getElementById("userparam-position").ownText());
    return userWithJsessionId;
  }
}
