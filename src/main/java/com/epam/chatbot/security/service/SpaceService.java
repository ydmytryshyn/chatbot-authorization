package com.epam.chatbot.security.service;

import com.epam.chatbot.security.model.Space;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The type Space service.
 */
@Service
public class SpaceService {

  /**
   * The Json parser service.
   */
  @Autowired
  JsonParserService jsonParserService;

  /**
   * Gets spaces from kb.
   *
   * @return the spaces from kb
   * @throws IOException the io exception
   */
  public List<Space> getSpacesFromKb() throws IOException {
    List<Space> spaces = new ArrayList<>();
    String text = readLineByLineFromFile("spaces.xml");
    Document document = Jsoup.parse(text);
    Elements elements = document.getElementsByAttribute("key");
    for (Element element : elements) {
      String key = element.attr("key");
      String name = element.attr("name");
      spaces.add(new Space(key, name));
    }
    return spaces;
  }

  private String readLineByLineFromFile(String fileName) {
    StringBuilder result = new StringBuilder("");

    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource(fileName).getFile());

    try (Scanner scanner = new Scanner(file)) {

      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        result.append(line).append("\n");
      }

      scanner.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
    return result.toString();

  }
}
