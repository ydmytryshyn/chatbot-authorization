package com.epam.chatbot.security.service.util;

import java.util.Date;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;

/**
 * The type Custom persistent remember me token.
 */
public class CustomPersistentRememberMeToken extends PersistentRememberMeToken {

  private final String kbSessionId;

  /**
   * Instantiates a new Custom persistent remember me token.
   *
   * @param username the username
   * @param series the series
   * @param tokenValue the token value
   * @param date the date
   * @param kbSessionId the kb session id
   */
  public CustomPersistentRememberMeToken(String username, String series, String tokenValue,
      Date date, String kbSessionId) {
    super(username, series, tokenValue, date);
    this.kbSessionId = kbSessionId;
  }

  /**
   * Gets kb session id.
   *
   * @return the kb session id
   */
  public String getKbSessionId() {
    return this.kbSessionId;
  }
}
