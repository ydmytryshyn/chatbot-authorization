package com.epam.chatbot.security.service;


import com.epam.chatbot.security.luis.Entity;
import com.epam.chatbot.security.luis.LuisResponse;
import com.epam.chatbot.security.utils.IntentTypes;
import com.epam.chatbot.security.utils.Urls;
import com.epam.chatbot.security.utils.UserDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Victor on 04.07.2018.
 * Зробимо в четвер
 */
@Component
public class TextProcessorImpl implements TextProcessor {


  @Autowired
  private ObjectMapper objectMapper;


  /**
   * This method uses LUIS to perform Language Understanding actions
   * based on it`s reply we decide what intent does user have
   */
  @Override
  public void decide(UserDetails userDetails) {
    String question = userDetails.getActivity().getText();
    LuisResponse response;
    HttpResponse<JsonNode> jsonResponse = null;
    try {
      jsonResponse = createRequest(question).asJson();
    } catch (UnirestException e) {
      e.printStackTrace();
    }
    response = deserializeResponse(jsonResponse.getBody().toString());
    IntentTypes intent = IntentTypes
        .valueOf(response.getTopScoringIntent().getIntent().toUpperCase());

    if (intent.equals(IntentTypes.SEARCH)) {
      Map<String, Pair<String, String>> requeuestParams = getRequestParams(response);
      for (String s :
          requeuestParams.keySet()) {
        System.err.println(s + " " + requeuestParams.get(s));
      }
      if (requeuestParams.isEmpty()) {
        userDetails.getUser().setIntentType(IntentTypes.GET_ARTICLE);
      } else {
        userDetails.getUser().setIntentType(IntentTypes.GET_ARTICLE_PARAMS);
        userDetails.setRequestParams(requeuestParams);
      }
    } else if (intent.equals(IntentTypes.CONFIRMATION)) {
      userDetails.getReply().setConfirmed(true);
    } else {
      userDetails.getUser().setIntentType(
          IntentTypes.valueOf(response.getTopScoringIntent().getIntent().toUpperCase()));
    }
  }


  private Map<String, Pair<String, String>> getRequestParams(LuisResponse response) {

    Map<String, Pair<String, String>> requestParams = new HashMap<>(3);
    for (Entity entity : response.getEntities()) {
      if (entity.getType().equals("Space")) {
        requestParams.put("Space", new Pair<>("+and+space+=", entity.getEntity()));
      }
      if (entity.getType().equals("Article")) {

        requestParams.put("Post", new Pair<>("siteSearch+~+", entity.getEntity()));
      }
      if (entity.getType().equals("Author")) {
        requestParams.put("Author", new Pair<>("+and+contributor+=+", entity.getEntity()));
      }
    }
    String space = parseSpace(response.getQuery());
    if (space != null) {
      requestParams.put("Space", new Pair<>("+and+space+=", space));
    }
    String post = parsePost(response.getQuery());
    if (post != null) {
      requestParams.put("Post", new Pair<>("siteSearch+~+", post));
    }
    String author = parseAuthor(response.getQuery());
    if (author != null) {
      requestParams.put("Author", new Pair<>("+and+contributor+=+", author));
    }
    return requestParams;
  }

  /**
   * Method creates new request to LUIS API
   *
   * @param question - user's question
   */
  private HttpRequest createRequest(String question) {
    HttpRequest request = Unirest.get(Urls.RECOGNISION
        .getUrl())
        .queryString("q", question);
    return request;
  }

  /**
   * Method constructs new LuisResponse object from json
   *
   * @param body json string, which if obtained from LUIS
   */
  private LuisResponse deserializeResponse(String body) {
    try {
      return objectMapper.readValue(body, LuisResponse.class);
    } catch (IOException e) {
      return null;
    }
  }


  /**
   * This method finds Space in the input message
   *
   * @param message - LUIS query
   */
  private String parseSpace(String message) {
    Pattern patternCity = Pattern.compile("Space:([A-Za-z_]*)");
    Matcher matcher = patternCity.matcher(message);
    String s = null;
    if (matcher.find()) {
      s = matcher.group().replace('_', ' ').split(":")[1];
    }
    return s;
  }

  private String parseAuthor(String message) {
    Pattern patternCity = Pattern.compile("Author:([A-Za-z_]*)");
    Matcher matcher = patternCity.matcher(message);
    String s = null;
    if (matcher.find()) {
      s = matcher.group().replace('_', ' ').split(":")[1];
    }
    return s;
  }

  private String parsePost(String message) {
    Pattern patternCity = Pattern.compile("Post:([A-Za-z_]*)");
    Matcher matcher = patternCity.matcher(message);
    String s = null;
    if (matcher.find()) {
      s = matcher.group().replace('_', ' ').split(":")[1];
    }
    return s;
  }

}
