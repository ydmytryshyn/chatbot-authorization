package com.epam.chatbot.security.service;

import com.epam.chatbot.security.model.Reply;
import com.epam.chatbot.security.repository.ReplyRepository;
import com.epam.chatbot.security.utils.UserDetails;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * The type Reply service.
 */
@Service
public class ReplyService {

  @Autowired
  private ReplyRepository replyRepository;


  /**
   * Get replyes for intent list.
   *
   * @param userDetails the user details
   * @return the list
   */
  public List<Reply> getReplyesForIntent(UserDetails userDetails) {

    return replyRepository.findAllByIntentType(userDetails.getUser().getIntentType());

  }
}
