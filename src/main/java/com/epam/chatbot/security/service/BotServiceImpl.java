package com.epam.chatbot.security.service;

import com.epam.chatbot.security.model.Activity;
import com.epam.chatbot.security.utils.UserCache;
import com.epam.chatbot.security.utils.UserDetails;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The type Bot service.
 */
@Service
public class BotServiceImpl implements BotService {


  @Autowired
  private TextProcessor textProcessor;

  @Autowired
  private ReplyService replyService;

  @Autowired
  private JsonParserService jsonParserService;
  @Autowired
  private SpaceService spaceService;
  private ResponseChainFormer responseChainFormer;

  private boolean inited;


  private void init() {
    responseChainFormer = new ResponseChainFormer(replyService, jsonParserService, spaceService);
    inited = true;
  }

  @Override
  public Activity interactWithBot(UserDetails userDetails) {
    if (!inited) {
      init();
    }
    if (userDetails.getActivity() != null) {

      /**
       * Text processor will analyze request and decide what answer give to customer
       */
      textProcessor.decide(userDetails);
      /**
       * Save User state to Cache
       */
      /**
       * Performing actions that needed
       */
      responseChainFormer.formAnswer(userDetails);

      UserCache userCache = UserCache.getInstance();
      try {
        userCache.putObject(userDetails);
      } catch (IOException e) {
        e.printStackTrace();
      }

      System.out.println("Sending Reply:  " + userDetails.getReply().getReply());
      try {
        return ActivityFactory.createReply(userDetails.getReply().getReply());
      } catch (Exception ex) {
        System.out.println("Failed to handle message");
      }
    }
    return null;
  }
}

