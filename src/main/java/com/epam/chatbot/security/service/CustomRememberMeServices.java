package com.epam.chatbot.security.service;

import com.epam.chatbot.security.model.UserWithJsessionId;
import com.epam.chatbot.security.service.util.CustomJdbcTokenRepositoryImpl;
import com.epam.chatbot.security.service.util.CustomPersistentRememberMeToken;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.security.web.authentication.rememberme.CookieTheftException;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationException;
import org.springframework.util.Assert;

/**
 * The type Custom remember me services.
 */
public class CustomRememberMeServices extends AbstractRememberMeServices {

  private CustomJdbcTokenRepositoryImpl tokenRepository = null;
  private SecureRandom random;

  /**
   * The constant DEFAULT_SERIES_LENGTH.
   */
  public static final int DEFAULT_SERIES_LENGTH = 16;
  /**
   * The constant DEFAULT_TOKEN_LENGTH.
   */
  public static final int DEFAULT_TOKEN_LENGTH = 16;

  private int seriesLength = DEFAULT_SERIES_LENGTH;
  private int tokenLength = DEFAULT_TOKEN_LENGTH;

  /**
   * Instantiates a new Custom remember me services.
   *
   * @param key the key
   * @param tokenRepository the token repository
   */
  public CustomRememberMeServices(String key,
      CustomJdbcTokenRepositoryImpl tokenRepository) {
    super(key, new UserDetailsService() {
      @Override
      public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        throw new UsernameNotFoundException("This method should not be called.");
      }
    });
    this.setUserDetailsChecker(new CustomUserDetailsChecker());
    random = new SecureRandom();
    this.tokenRepository = tokenRepository;
  }

  protected UserDetails processAutoLoginCookie(String[] cookieTokens,
      HttpServletRequest request, HttpServletResponse response) {

    if (cookieTokens.length != 2) {
      throw new InvalidCookieException("Cookie token did not contain " + 2
          + " tokens, but contained '" + Arrays.asList(cookieTokens) + "'");
    }

    final String presentedSeries = cookieTokens[0];
    final String presentedToken = cookieTokens[1];

    CustomPersistentRememberMeToken token = tokenRepository
        .getTokenForSeries(presentedSeries);

    if (token == null) {
      // No series match, so we can't authenticate using this cookie
      throw new RememberMeAuthenticationException(
          "No persistent token found for series id: " + presentedSeries);
    }

    // We have a match for this user/series combination
    if (!presentedToken.equals(token.getTokenValue())) {
      // Token doesn't match series value. Delete all logins for this user and throw
      // an exception to warn them.
      tokenRepository.removeUserTokens(token.getUsername());

      throw new CookieTheftException(
          messages.getMessage(
              "PersistentTokenBasedRememberMeServices.cookieStolen",
              "Invalid remember-me token (Series/token) mismatch. Implies previous cookie theft attack."));
    }

    if (token.getDate().getTime() + getTokenValiditySeconds() * 1000L < System
        .currentTimeMillis()) {
      throw new RememberMeAuthenticationException("Remember-me login has expired");
    }

    // Token also matches, so login is valid. Update the token value, keeping the
    // *same* series number.
    if (logger.isDebugEnabled()) {
      logger.debug("Refreshing persistent login token for user '"
          + token.getUsername() + "', series '" + token.getSeries() + "'");
    }

    PersistentRememberMeToken newToken = new PersistentRememberMeToken(
        token.getUsername(), token.getSeries(), generateTokenData(), new Date());

    try {
      tokenRepository.updateToken(newToken.getSeries(), newToken.getTokenValue(),
          newToken.getDate());
      addCookie(newToken, request, response);
    } catch (Exception e) {
      logger.error("Failed to update token: ", e);
      throw new RememberMeAuthenticationException(
          "Autologin failed due to data access problem");
    }

    final List<GrantedAuthority> grantedAuths = new ArrayList<>();
    UserDetails userDetails = new UserWithJsessionId(token.getUsername(), "dummy", grantedAuths,
        token.getKbSessionId());
    return userDetails;
  }

  protected void onLoginSuccess(HttpServletRequest request,
      HttpServletResponse response, Authentication successfulAuthentication) {
    UserWithJsessionId userWithJsessionId = (UserWithJsessionId) successfulAuthentication
        .getPrincipal();

    String username = userWithJsessionId.getUsername();
    String kbSessionId = userWithJsessionId.getKbJsessionId();

    logger.debug("Creating new persistent login for user " + username);

    CustomPersistentRememberMeToken persistentToken = new CustomPersistentRememberMeToken(
        username, generateSeriesData(), generateTokenData(), new Date(), kbSessionId);
    try {
      tokenRepository.createNewToken(persistentToken);
      addCookie(persistentToken, request, response);
    } catch (Exception e) {
      logger.error("Failed to save persistent token ", e);
    }
  }

  @Override
  public void logout(HttpServletRequest request, HttpServletResponse response,
      Authentication authentication) {
    super.logout(request, response, authentication);

    if (authentication != null) {
      tokenRepository.removeUserTokens(authentication.getName());
    }
  }

  @Override
  protected Authentication createSuccessfulAuthentication(HttpServletRequest request,
      UserDetails user) {
    final List<GrantedAuthority> grantedAuths = new ArrayList<>();
    RememberMeAuthenticationToken auth = new RememberMeAuthenticationToken(getKey(), user,
        grantedAuths);
    auth.setDetails(getAuthenticationDetailsSource().buildDetails(request));
    return auth;
  }

  /**
   * Generate series data string.
   *
   * @return the string
   */
  protected String generateSeriesData() {
    byte[] newSeries = new byte[seriesLength];
    random.nextBytes(newSeries);
    return new String(Base64.getEncoder().encode(newSeries));
  }

  /**
   * Generate token data string.
   *
   * @return the string
   */
  protected String generateTokenData() {
    byte[] newToken = new byte[tokenLength];
    random.nextBytes(newToken);
    return new String(Base64.getEncoder().encode(newToken));
  }

  private void addCookie(PersistentRememberMeToken token, HttpServletRequest request,
      HttpServletResponse response) {
    setCookie(new String[]{token.getSeries(), token.getTokenValue()},
        getTokenValiditySeconds(), request, response);
  }

  /**
   * Sets series length.
   *
   * @param seriesLength the series length
   */
  public void setSeriesLength(int seriesLength) {
    this.seriesLength = seriesLength;
  }

  /**
   * Sets token length.
   *
   * @param tokenLength the token length
   */
  public void setTokenLength(int tokenLength) {
    this.tokenLength = tokenLength;
  }

  @Override
  public void setTokenValiditySeconds(int tokenValiditySeconds) {
    Assert.isTrue(tokenValiditySeconds > 0,
        "tokenValiditySeconds must be positive for this implementation");
    super.setTokenValiditySeconds(tokenValiditySeconds);
  }

  private static class CustomUserDetailsChecker implements UserDetailsChecker {

    /**
     * The Messages.
     */
    protected final MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

    /**
     * Instantiates a new Custom user details checker.
     */
    public CustomUserDetailsChecker() {
    }

    public void check(UserDetails user) {
    }
  }
}