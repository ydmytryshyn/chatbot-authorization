package com.epam.chatbot.security.service;


import com.epam.chatbot.security.bot_actions.Action;
import com.epam.chatbot.security.bot_actions.ArticleParametersAction;
import com.epam.chatbot.security.bot_actions.NoneAction;
import com.epam.chatbot.security.bot_actions.RequestArticleAction;
import com.epam.chatbot.security.bot_actions.WelcomeAction;
import com.epam.chatbot.security.utils.IntentTypes;
import com.epam.chatbot.security.utils.UserDetails;
import java.util.HashMap;
import java.util.Map;


/**
 * The type Response chain former.
 */
public class ResponseChainFormer {

  private Map<IntentTypes, Action> reactions = new HashMap<>();

  /**
   * Instantiates a new Response chain former.
   *
   * @param replyService the reply service
   * @param jsonParserService the json parser service
   * @param spaceService the space service
   */
  public ResponseChainFormer(ReplyService replyService, JsonParserService jsonParserService,
      SpaceService spaceService) {
    reactions.put(IntentTypes.GET_ARTICLE_PARAMS,
        new ArticleParametersAction(replyService, jsonParserService, spaceService));
    reactions.put(IntentTypes.GET_ARTICLE, new RequestArticleAction());
    reactions.put(IntentTypes.HELLO, new WelcomeAction(replyService));
    reactions.put(IntentTypes.NONE, new NoneAction(replyService));
  }

  /**
   * Form answer.
   *
   * @param userDetails the user details
   */
  public void formAnswer(UserDetails userDetails) {
    System.out.println(userDetails.getUser().getIntentType().name());
    reactions.get(userDetails.getUser().getIntentType()).action(userDetails);
  }
}
