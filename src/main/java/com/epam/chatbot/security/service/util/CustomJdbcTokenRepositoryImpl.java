package com.epam.chatbot.security.service.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 * The type Custom jdbc token repository.
 */
public class CustomJdbcTokenRepositoryImpl extends JdbcDaoSupport {

  /**
   * Default SQL for creating the database table to store the tokens
   */
  public static final String CREATE_TABLE_SQL =
      "create table persistent_logins (username varchar(200) not null, series varchar(64) primary key, "
          + "token varchar(64) not null, last_used timestamp not null, kb_session_id varchar(300) not null)";
  /**
   * The default SQL used by the <tt>getTokenBySeries</tt> query
   */
  public static final String DEF_TOKEN_BY_SERIES_SQL =
      "select username,series,token,last_used,kb_session_id from persistent_logins where series = ?";
  /**
   * The default SQL used by <tt>createNewToken</tt>
   */
  public static final String DEF_INSERT_TOKEN_SQL =
      "insert into persistent_logins (username, series, token, last_used, kb_session_id) values(?,?,?,?,?)";
  /**
   * The default SQL used by <tt>updateToken</tt>
   */
  public static final String DEF_UPDATE_TOKEN_SQL =
      "update persistent_logins set token = ?, last_used = ? where series = ?";
  /**
   * The default SQL used by <tt>removeUserTokens</tt>
   */
  public static final String DEF_REMOVE_USER_TOKENS_SQL =
      "delete from persistent_logins where username = ?";

  private String tokensBySeriesSql = DEF_TOKEN_BY_SERIES_SQL;
  private String insertTokenSql = DEF_INSERT_TOKEN_SQL;
  private String updateTokenSql = DEF_UPDATE_TOKEN_SQL;
  private String removeUserTokensSql = DEF_REMOVE_USER_TOKENS_SQL;
  private boolean createTableOnStartup;

  protected void initDao() {
    if (createTableOnStartup) {
      getJdbcTemplate().execute(CREATE_TABLE_SQL);
    }
  }

  /**
   * Create new token.
   *
   * @param token the token
   */
  public void createNewToken(CustomPersistentRememberMeToken token) {
    getJdbcTemplate()
        .update(insertTokenSql, token.getUsername(), token.getSeries(), token.getTokenValue(),
            token.getDate(), token.getKbSessionId());

    if (logger.isDebugEnabled()) {
      logger.debug("Created token for user '" + token.getUsername() + "' .");
    }
  }

  /**
   * Update token.
   *
   * @param series the series
   * @param tokenValue the token value
   * @param lastUsed the last used
   */
  public void updateToken(String series, String tokenValue, Date lastUsed) {
    getJdbcTemplate().update(updateTokenSql, tokenValue, lastUsed, series);
  }

  /**
   * Gets token for series.
   *
   * @param seriesId the series id
   * @return the token for series
   */
  public CustomPersistentRememberMeToken getTokenForSeries(String seriesId) {
    try {
      return getJdbcTemplate().queryForObject(tokensBySeriesSql,
          new RowMapper<CustomPersistentRememberMeToken>() {
            public CustomPersistentRememberMeToken mapRow(ResultSet rs, int rowNum)
                throws SQLException {
              return new CustomPersistentRememberMeToken(rs.getString(1), rs
                  .getString(2), rs.getString(3), rs.getTimestamp(4), rs.getString(5));
            }
          }, seriesId);
    } catch (EmptyResultDataAccessException zeroResults) {
      if (logger.isDebugEnabled()) {
        logger.debug("Querying token for series '" + seriesId
            + "' returned no results.", zeroResults);
      }
    } catch (IncorrectResultSizeDataAccessException moreThanOne) {
      logger.error("Querying token for series '" + seriesId
          + "' returned more than one value. Series" + " should be unique");
    } catch (DataAccessException e) {
      logger.error("Failed to load token for series " + seriesId, e);
    }

    return null;
  }

  /**
   * Remove user tokens.
   *
   * @param username the username
   */
  public void removeUserTokens(String username) {
    getJdbcTemplate().update(removeUserTokensSql, username);

    if (logger.isDebugEnabled()) {
      logger.debug("Removed token for user '" + username + "' .");
    }
  }

  /**
   * Sets create table on startup.
   *
   * @param createTableOnStartup the create table on startup
   */
  public void setCreateTableOnStartup(boolean createTableOnStartup) {
    this.createTableOnStartup = createTableOnStartup;
  }
}