package com.epam.chatbot.security.service;

import com.epam.chatbot.security.error.KbAuthenticationServiceException;
import com.epam.chatbot.security.model.UserWithJsessionId;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 * The type Authentication provider.
 */
@Service("authenticationProvider")
public class AuthenticationProviderImpl implements AuthenticationProvider {


  public static final Logger logger = LoggerFactory.getLogger(AuthenticationProviderImpl.class);


  @Autowired
  KbAuthenticationService kbAuthenticationService;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {

    final String name = authentication.getName();
    final String password = authentication.getCredentials().toString();
    String kbJsessionId = "";

    logger.debug("New user authenticated: {}", name);

    try {
      kbJsessionId = kbAuthenticationService.authenticate(name, password);
    } catch (KbAuthenticationServiceException e) {
      throw new BadCredentialsException("KB authentication failed");
    }

    final List<GrantedAuthority> grantedAuths = new ArrayList<>();
    final UserDetails principal = new UserWithJsessionId(name, password, grantedAuths,
        kbJsessionId);
    final Authentication auth = new UsernamePasswordAuthenticationToken(principal, password,
        grantedAuths);

    return auth;
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return aClass.equals(UsernamePasswordAuthenticationToken.class);
  }
}
