package com.epam.chatbot.security.service;

import com.epam.chatbot.security.model.Activity;
import com.epam.chatbot.security.utils.UserDetails;

/**
 * The interface Bot service.
 */
public interface BotService {

  /**
   * Interact with bot activity.
   *
   * @param userDetails the user details
   * @return the activity
   */
  Activity interactWithBot(UserDetails userDetails);
}
