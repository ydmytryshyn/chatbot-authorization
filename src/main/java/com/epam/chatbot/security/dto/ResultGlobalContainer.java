
package com.epam.chatbot.security.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;

/**
 * The type Result global container.
 */
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "title",
    "displayUrl"
})
public class ResultGlobalContainer {

  @JsonProperty("title")
  private String title;
  @JsonProperty("displayUrl")
  private String displayUrl;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  /**
   * Gets title.
   *
   * @return the title
   */
  @JsonProperty("title")
  public String getTitle() {
    return title;
  }

  /**
   * Sets title.
   *
   * @param title the title
   */
  @JsonProperty("title")
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * Gets display url.
   *
   * @return the display url
   */
  @JsonProperty("displayUrl")
  public String getDisplayUrl() {
    return displayUrl;
  }

  /**
   * Sets display url.
   *
   * @param displayUrl the display url
   */
  @JsonProperty("displayUrl")
  public void setDisplayUrl(String displayUrl) {
    this.displayUrl = displayUrl;
  }

  /**
   * Gets additional properties.
   *
   * @return the additional properties
   */
  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  /**
   * Sets additional property.
   *
   * @param name the name
   * @param value the value
   */
  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

}
