
package com.epam.chatbot.security.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Links.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "self",
    "webui",
    "tinyui"
})
public class Links {

  @JsonProperty("self")
  private String self;
  @JsonProperty("webui")
  private String webui;
  @JsonProperty("tinyui")
  private String tinyui;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  /**
   * Gets self.
   *
   * @return the self
   */
  @JsonProperty("self")
  public String getSelf() {
    return self;
  }

  /**
   * Sets self.
   *
   * @param self the self
   */
  @JsonProperty("self")
  public void setSelf(String self) {
    this.self = self;
  }

  /**
   * Gets webui.
   *
   * @return the webui
   */
  @JsonProperty("webui")
  public String getWebui() {
    return webui;
  }

  /**
   * Sets webui.
   *
   * @param webui the webui
   */
  @JsonProperty("webui")
  public void setWebui(String webui) {
    this.webui = webui;
  }

  /**
   * Gets tinyui.
   *
   * @return the tinyui
   */
  @JsonProperty("tinyui")
  public String getTinyui() {
    return tinyui;
  }

  /**
   * Sets tinyui.
   *
   * @param tinyui the tinyui
   */
  @JsonProperty("tinyui")
  public void setTinyui(String tinyui) {
    this.tinyui = tinyui;
  }

  /**
   * Gets additional properties.
   *
   * @return the additional properties
   */
  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  /**
   * Sets additional property.
   *
   * @param name the name
   * @param value the value
   */
  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

}
