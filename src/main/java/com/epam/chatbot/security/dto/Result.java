
package com.epam.chatbot.security.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Result.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "content",
    "title",
    "excerpt",
    "url",
    "resultGlobalContainer",
    "entityType",
    "iconCssClass",
    "lastModified",
    "friendlyLastModified",
    "timestamp"
})
public class Result {

  @JsonProperty("content")
  private Content content;
  @JsonProperty("title")
  private String title;
  @JsonProperty("excerpt")
  private String excerpt;
  @JsonProperty("url")
  private String url;
  @JsonProperty("resultGlobalContainer")
  private ResultGlobalContainer resultGlobalContainer;
  @JsonProperty("entityType")
  private String entityType;
  @JsonProperty("iconCssClass")
  private String iconCssClass;
  @JsonProperty("lastModified")
  private String lastModified;
  @JsonProperty("friendlyLastModified")
  private String friendlyLastModified;
  @JsonProperty("timestamp")
  private Long timestamp;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  /**
   * Gets content.
   *
   * @return the content
   */
  @JsonProperty("content")
  public Content getContent() {
    return content;
  }

  /**
   * Sets content.
   *
   * @param content the content
   */
  @JsonProperty("content")
  public void setContent(Content content) {
    this.content = content;
  }

  /**
   * Gets title.
   *
   * @return the title
   */
  @JsonProperty("title")
  public String getTitle() {
    return title;
  }

  /**
   * Sets title.
   *
   * @param title the title
   */
  @JsonProperty("title")
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * Gets excerpt.
   *
   * @return the excerpt
   */
  @JsonProperty("excerpt")
  public String getExcerpt() {
    return excerpt;
  }

  /**
   * Sets excerpt.
   *
   * @param excerpt the excerpt
   */
  @JsonProperty("excerpt")
  public void setExcerpt(String excerpt) {
    this.excerpt = excerpt;
  }

  /**
   * Gets url.
   *
   * @return the url
   */
  @JsonProperty("url")
  public String getUrl() {
    return url;
  }

  /**
   * Sets url.
   *
   * @param url the url
   */
  @JsonProperty("url")
  public void setUrl(String url) {
    this.url = url;
  }

  /**
   * Gets result global container.
   *
   * @return the result global container
   */
  @JsonProperty("resultGlobalContainer")
  public ResultGlobalContainer getResultGlobalContainer() {
    return resultGlobalContainer;
  }

  /**
   * Sets result global container.
   *
   * @param resultGlobalContainer the result global container
   */
  @JsonProperty("resultGlobalContainer")
  public void setResultGlobalContainer(ResultGlobalContainer resultGlobalContainer) {
    this.resultGlobalContainer = resultGlobalContainer;
  }

  /**
   * Gets entity type.
   *
   * @return the entity type
   */
  @JsonProperty("entityType")
  public String getEntityType() {
    return entityType;
  }

  /**
   * Sets entity type.
   *
   * @param entityType the entity type
   */
  @JsonProperty("entityType")
  public void setEntityType(String entityType) {
    this.entityType = entityType;
  }

  /**
   * Gets icon css class.
   *
   * @return the icon css class
   */
  @JsonProperty("iconCssClass")
  public String getIconCssClass() {
    return iconCssClass;
  }

  /**
   * Sets icon css class.
   *
   * @param iconCssClass the icon css class
   */
  @JsonProperty("iconCssClass")
  public void setIconCssClass(String iconCssClass) {
    this.iconCssClass = iconCssClass;
  }

  /**
   * Gets last modified.
   *
   * @return the last modified
   */
  @JsonProperty("lastModified")
  public String getLastModified() {
    return lastModified;
  }

  /**
   * Sets last modified.
   *
   * @param lastModified the last modified
   */
  @JsonProperty("lastModified")
  public void setLastModified(String lastModified) {
    this.lastModified = lastModified;
  }

  /**
   * Gets friendly last modified.
   *
   * @return the friendly last modified
   */
  @JsonProperty("friendlyLastModified")
  public String getFriendlyLastModified() {
    return friendlyLastModified;
  }

  /**
   * Sets friendly last modified.
   *
   * @param friendlyLastModified the friendly last modified
   */
  @JsonProperty("friendlyLastModified")
  public void setFriendlyLastModified(String friendlyLastModified) {
    this.friendlyLastModified = friendlyLastModified;
  }

  /**
   * Gets timestamp.
   *
   * @return the timestamp
   */
  @JsonProperty("timestamp")
  public Long getTimestamp() {
    return timestamp;
  }

  /**
   * Sets timestamp.
   *
   * @param timestamp the timestamp
   */
  @JsonProperty("timestamp")
  public void setTimestamp(Long timestamp) {
    this.timestamp = timestamp;
  }

  /**
   * Gets additional properties.
   *
   * @return the additional properties
   */
  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  /**
   * Sets additional property.
   *
   * @param name the name
   * @param value the value
   */
  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

}
