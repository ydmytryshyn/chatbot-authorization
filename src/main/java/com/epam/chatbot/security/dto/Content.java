
package com.epam.chatbot.security.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Content.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    // "id",
    "type",
    "status",
    "title",
    "restrictions",
    "_expandable",
    "_links"
})
public class Content {

  //    @JsonProperty("id")
//    private Long id;
  @JsonProperty("type")
  private String type;
  @JsonProperty("status")
  private String status;
  @JsonProperty("title")
  private String title;
  @JsonProperty("restrictions")
  private Restrictions restrictions;
  @JsonProperty("_expandable")
  private Expandable expandable;
  @JsonProperty("_links")
  private Links links;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();


  /**
   * Gets type.
   *
   * @return the type
   */
  @JsonProperty("type")
  public String getType() {
    return type;
  }

  /**
   * Sets type.
   *
   * @param type the type
   */
  @JsonProperty("type")
  public void setType(String type) {
    this.type = type;
  }

  /**
   * Gets status.
   *
   * @return the status
   */
  @JsonProperty("status")
  public String getStatus() {
    return status;
  }

  /**
   * Sets status.
   *
   * @param status the status
   */
  @JsonProperty("status")
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * Gets title.
   *
   * @return the title
   */
  @JsonProperty("title")
  public String getTitle() {
    return title;
  }

  /**
   * Sets title.
   *
   * @param title the title
   */
  @JsonProperty("title")
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * Gets restrictions.
   *
   * @return the restrictions
   */
  @JsonProperty("restrictions")
  public Restrictions getRestrictions() {
    return restrictions;
  }

  /**
   * Sets restrictions.
   *
   * @param restrictions the restrictions
   */
  @JsonProperty("restrictions")
  public void setRestrictions(Restrictions restrictions) {
    this.restrictions = restrictions;
  }

  /**
   * Gets expandable.
   *
   * @return the expandable
   */
  @JsonProperty("_expandable")
  public Expandable getExpandable() {
    return expandable;
  }

  /**
   * Sets expandable.
   *
   * @param expandable the expandable
   */
  @JsonProperty("_expandable")
  public void setExpandable(Expandable expandable) {
    this.expandable = expandable;
  }

  /**
   * Gets links.
   *
   * @return the links
   */
  @JsonProperty("_links")
  public Links getLinks() {
    return links;
  }

  /**
   * Sets links.
   *
   * @param links the links
   */
  @JsonProperty("_links")
  public void setLinks(Links links) {
    this.links = links;
  }

  /**
   * Gets additional properties.
   *
   * @return the additional properties
   */
  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  /**
   * Sets additional property.
   *
   * @param name the name
   * @param value the value
   */
  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

}
