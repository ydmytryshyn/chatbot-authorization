package com.epam.chatbot.security.dto;

import com.epam.chatbot.security.model.UserWithJsessionId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The type Dto util mapper.
 */
@Component
public class DTOUtilMapper {

  @Autowired
  private ModelMapper modelMapper;


  /**
   * Convert to dto user with jsession dto.
   *
   * @param userWithJsessionId the user with jsession id
   * @return the user with jsession dto
   */
  public UserWithJsessionDTO convertToDto(UserWithJsessionId userWithJsessionId) {
    UserWithJsessionDTO userWithJsessionDTO = modelMapper
        .map(userWithJsessionId, UserWithJsessionDTO.class);
    return userWithJsessionDTO;
  }

}
