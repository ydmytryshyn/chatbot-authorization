
package com.epam.chatbot.security.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Links.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "base",
    "context"
})
public class Links_ {

  @JsonProperty("base")
  private String base;
  @JsonProperty("context")
  private String context;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  /**
   * Gets base.
   *
   * @return the base
   */
  @JsonProperty("base")
  public String getBase() {
    return base;
  }

  /**
   * Sets base.
   *
   * @param base the base
   */
  @JsonProperty("base")
  public void setBase(String base) {
    this.base = base;
  }

  /**
   * Gets context.
   *
   * @return the context
   */
  @JsonProperty("context")
  public String getContext() {
    return context;
  }

  /**
   * Sets context.
   *
   * @param context the context
   */
  @JsonProperty("context")
  public void setContext(String context) {
    this.context = context;
  }

  /**
   * Gets additional properties.
   *
   * @return the additional properties
   */
  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  /**
   * Sets additional property.
   *
   * @param name the name
   * @param value the value
   */
  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

}
