
package com.epam.chatbot.security.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Expandable.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "container",
    "metadata",
    "extensions",
    "operations",
    "children",
    "history",
    "ancestors",
    "body",
    "version",
    "descendants",
    "space"
})
public class Expandable {

  @JsonProperty("container")
  private String container;
  @JsonProperty("metadata")
  private String metadata;
  @JsonProperty("extensions")
  private String extensions;
  @JsonProperty("operations")
  private String operations;
  @JsonProperty("children")
  private String children;
  @JsonProperty("history")
  private String history;
  @JsonProperty("ancestors")
  private String ancestors;
  @JsonProperty("body")
  private String body;
  @JsonProperty("version")
  private String version;
  @JsonProperty("descendants")
  private String descendants;
  @JsonProperty("space")
  private String space;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  /**
   * Gets container.
   *
   * @return the container
   */
  @JsonProperty("container")
  public String getContainer() {
    return container;
  }

  /**
   * Sets container.
   *
   * @param container the container
   */
  @JsonProperty("container")
  public void setContainer(String container) {
    this.container = container;
  }

  /**
   * Gets metadata.
   *
   * @return the metadata
   */
  @JsonProperty("metadata")
  public String getMetadata() {
    return metadata;
  }

  /**
   * Sets metadata.
   *
   * @param metadata the metadata
   */
  @JsonProperty("metadata")
  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  /**
   * Gets extensions.
   *
   * @return the extensions
   */
  @JsonProperty("extensions")
  public String getExtensions() {
    return extensions;
  }

  /**
   * Sets extensions.
   *
   * @param extensions the extensions
   */
  @JsonProperty("extensions")
  public void setExtensions(String extensions) {
    this.extensions = extensions;
  }

  /**
   * Gets operations.
   *
   * @return the operations
   */
  @JsonProperty("operations")
  public String getOperations() {
    return operations;
  }

  /**
   * Sets operations.
   *
   * @param operations the operations
   */
  @JsonProperty("operations")
  public void setOperations(String operations) {
    this.operations = operations;
  }

  /**
   * Gets children.
   *
   * @return the children
   */
  @JsonProperty("children")
  public String getChildren() {
    return children;
  }

  /**
   * Sets children.
   *
   * @param children the children
   */
  @JsonProperty("children")
  public void setChildren(String children) {
    this.children = children;
  }

  /**
   * Gets history.
   *
   * @return the history
   */
  @JsonProperty("history")
  public String getHistory() {
    return history;
  }

  /**
   * Sets history.
   *
   * @param history the history
   */
  @JsonProperty("history")
  public void setHistory(String history) {
    this.history = history;
  }

  /**
   * Gets ancestors.
   *
   * @return the ancestors
   */
  @JsonProperty("ancestors")
  public String getAncestors() {
    return ancestors;
  }

  /**
   * Sets ancestors.
   *
   * @param ancestors the ancestors
   */
  @JsonProperty("ancestors")
  public void setAncestors(String ancestors) {
    this.ancestors = ancestors;
  }

  /**
   * Gets body.
   *
   * @return the body
   */
  @JsonProperty("body")
  public String getBody() {
    return body;
  }

  /**
   * Sets body.
   *
   * @param body the body
   */
  @JsonProperty("body")
  public void setBody(String body) {
    this.body = body;
  }

  /**
   * Gets version.
   *
   * @return the version
   */
  @JsonProperty("version")
  public String getVersion() {
    return version;
  }

  /**
   * Sets version.
   *
   * @param version the version
   */
  @JsonProperty("version")
  public void setVersion(String version) {
    this.version = version;
  }

  /**
   * Gets descendants.
   *
   * @return the descendants
   */
  @JsonProperty("descendants")
  public String getDescendants() {
    return descendants;
  }

  /**
   * Sets descendants.
   *
   * @param descendants the descendants
   */
  @JsonProperty("descendants")
  public void setDescendants(String descendants) {
    this.descendants = descendants;
  }

  /**
   * Gets space.
   *
   * @return the space
   */
  @JsonProperty("space")
  public String getSpace() {
    return space;
  }

  /**
   * Sets space.
   *
   * @param space the space
   */
  @JsonProperty("space")
  public void setSpace(String space) {
    this.space = space;
  }

  /**
   * Gets additional properties.
   *
   * @return the additional properties
   */
  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  /**
   * Sets additional property.
   *
   * @param name the name
   * @param value the value
   */
  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

}
