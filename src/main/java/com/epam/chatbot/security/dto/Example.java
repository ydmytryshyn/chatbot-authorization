
package com.epam.chatbot.security.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

/**
 * The type Example.
 */
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "results",
    "start",
    "limit",
    "size",
    "totalSize",
    "cqlQuery",
    "searchDuration",
    "_links"
})
public class Example {

  @JsonProperty("results")
  private List<Result> results = null;
  @JsonProperty("start")
  private Integer start;
  @JsonProperty("limit")
  private Integer limit;
  @JsonProperty("size")
  private Integer size;
  @JsonProperty("totalSize")
  private Integer totalSize;
  @JsonProperty("cqlQuery")
  private String cqlQuery;
  @JsonProperty("searchDuration")
  private Integer searchDuration;
  @JsonProperty("_links")
  private Links_ links;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  /**
   * Gets results.
   *
   * @return the results
   */
  @JsonProperty("results")
  public List<Result> getResults() {
    return results;
  }

  /**
   * Sets results.
   *
   * @param results the results
   */
  @JsonProperty("results")
  public void setResults(List<Result> results) {
    this.results = results;
  }

  /**
   * Gets start.
   *
   * @return the start
   */
  @JsonProperty("start")
  public Integer getStart() {
    return start;
  }

  /**
   * Sets start.
   *
   * @param start the start
   */
  @JsonProperty("start")
  public void setStart(Integer start) {
    this.start = start;
  }

  /**
   * Gets limit.
   *
   * @return the limit
   */
  @JsonProperty("limit")
  public Integer getLimit() {
    return limit;
  }

  /**
   * Sets limit.
   *
   * @param limit the limit
   */
  @JsonProperty("limit")
  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  /**
   * Gets size.
   *
   * @return the size
   */
  @JsonProperty("size")
  public Integer getSize() {
    return size;
  }

  /**
   * Sets size.
   *
   * @param size the size
   */
  @JsonProperty("size")
  public void setSize(Integer size) {
    this.size = size;
  }

  /**
   * Gets total size.
   *
   * @return the total size
   */
  @JsonProperty("totalSize")
  public Integer getTotalSize() {
    return totalSize;
  }

  /**
   * Sets total size.
   *
   * @param totalSize the total size
   */
  @JsonProperty("totalSize")
  public void setTotalSize(Integer totalSize) {
    this.totalSize = totalSize;
  }

  /**
   * Gets cql query.
   *
   * @return the cql query
   */
  @JsonProperty("cqlQuery")
  public String getCqlQuery() {
    return cqlQuery;
  }

  /**
   * Sets cql query.
   *
   * @param cqlQuery the cql query
   */
  @JsonProperty("cqlQuery")
  public void setCqlQuery(String cqlQuery) {
    this.cqlQuery = cqlQuery;
  }

  /**
   * Gets search duration.
   *
   * @return the search duration
   */
  @JsonProperty("searchDuration")
  public Integer getSearchDuration() {
    return searchDuration;
  }

  /**
   * Sets search duration.
   *
   * @param searchDuration the search duration
   */
  @JsonProperty("searchDuration")
  public void setSearchDuration(Integer searchDuration) {
    this.searchDuration = searchDuration;
  }

  /**
   * Gets links.
   *
   * @return the links
   */
  @JsonProperty("_links")
  public Links_ getLinks() {
    return links;
  }

  /**
   * Sets links.
   *
   * @param links the links
   */
  @JsonProperty("_links")
  public void setLinks(Links_ links) {
    this.links = links;
  }

  /**
   * Gets additional properties.
   *
   * @return the additional properties
   */
  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  /**
   * Sets additional property.
   *
   * @param name the name
   * @param value the value
   */
  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

}
