package com.epam.chatbot.security.bot_actions;


import com.epam.chatbot.security.model.Reply;
import com.epam.chatbot.security.service.ReplyService;
import com.epam.chatbot.security.utils.UserDetails;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 * Created by Victor on 06.07.2018.
 * Reply for None bot filed requests
 */
@Service
public class NoneAction implements Action {


  private ReplyService replyService;

  /**
   * Instantiates a new None action.
   *
   * @param replyService the reply service
   */
  public NoneAction(ReplyService replyService) {
    this.replyService = replyService;
  }

  @Override
  public void action(UserDetails userDetails) {
    List<Reply> replies = replyService.getReplyesForIntent(userDetails);
    int i = (int) (Math.random() * replies.size() - 1);
    userDetails.setReply(replies.get(i));
  }
}
