package com.epam.chatbot.security.bot_actions;

import com.epam.chatbot.security.utils.UserDetails;


/**
 * The interface Action.
 */
public interface Action {

  /**
   * Action.
   *
   * @param userDetails the user details
   */
  void action(UserDetails userDetails);
}
