package com.epam.chatbot.security.bot_actions;


import com.epam.chatbot.security.model.Reply;
import com.epam.chatbot.security.service.ReplyService;
import com.epam.chatbot.security.utils.UserDetails;
import java.util.List;

/**
 * Created by Victor on 06.07.2018.
 * Welcome Replies
 */
public class WelcomeAction implements Action {

  private ReplyService replyService;

  /**
   * Instantiates a new Welcome action.
   *
   * @param replyService the reply service
   */
  public WelcomeAction(ReplyService replyService) {
    this.replyService = replyService;
  }

  @Override
  public void action(UserDetails userDetails) {
    if (replyService == null) {
      System.out.println("NUULLLLLL");
    }
    List<Reply> replies = replyService.getReplyesForIntent(userDetails);
    int i = (int) (Math.random() * replies.size() - 1);
    userDetails.setReply(replies.get(i));
  }
}
