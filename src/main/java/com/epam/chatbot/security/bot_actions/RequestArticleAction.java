package com.epam.chatbot.security.bot_actions;

import com.epam.chatbot.security.model.Reply;
import com.epam.chatbot.security.utils.IntentTypes;
import com.epam.chatbot.security.utils.UserDetails;


/**
 * The type Request article action.
 */
public class RequestArticleAction implements Action {

  @Override
  public void action(UserDetails userDetails) {
    Reply reply;
    if (userDetails.getReply().isConfirmed()) {
      reply = new Reply("Please enter params for search in form param:value." +
          "Replace each space in param by _.Follow this example:" +
          "I want to find book in Space:GDO_Public_KB by Author:" +
          "Vladimir_Kirov with name Post:Java_in_Action", IntentTypes.GET_ARTICLE_PARAMS);
    } else {
      reply = new Reply("Did i understand you correctly, you want get some Article from KB?",
          IntentTypes.CONFIRMATION);
    }
    userDetails.setReply(reply);
  }
}