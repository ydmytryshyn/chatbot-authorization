package com.epam.chatbot.security.bot_actions;

import com.epam.chatbot.security.dto.Result;
import com.epam.chatbot.security.model.Reply;
import com.epam.chatbot.security.model.Space;
import com.epam.chatbot.security.service.JsonParserService;
import com.epam.chatbot.security.service.ReplyService;
import com.epam.chatbot.security.service.SpaceService;
import com.epam.chatbot.security.utils.IntentTypes;
import com.epam.chatbot.security.utils.Urls;
import com.epam.chatbot.security.utils.UserDetails;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.util.Pair;
import org.springframework.http.HttpHeaders;


/**
 * The type Article parameters action.
 */
public class ArticleParametersAction implements Action {


  private static final int MAX_NUMBER_OF_PARAMS = 3;

  private ReplyService replyService;
  private JsonParserService jsonParserService;
  private SpaceService spaceService;


  /**
   * Instantiates a new Article parameters action.
   *
   * @param replyService the reply service
   * @param jsonParserService the json parser service
   * @param spaceService the space service
   */
  public ArticleParametersAction(ReplyService replyService, JsonParserService jsonParserService,
      SpaceService spaceService) {
    this.replyService = replyService;
    this.jsonParserService = jsonParserService;
    this.spaceService = spaceService;
  }

  @Override
  public void action(UserDetails userDetails) {
    Reply reply;
    boolean confirmed = userDetails.getReply().isConfirmed();
    if (confirmed) {
      userDetails.getUser().setIntentType(IntentTypes.CONFIRMATION);
      userDetails.getUser().setIntentType(IntentTypes.GET_ARTICLE);

      reply = requestToKB(userDetails);

    } else {
      if (userDetails.getRequestParams().size() != 0) {
        StringBuilder replyText = new StringBuilder();
        replyText.append("You want to make search by this params:  ");
        for (String s : userDetails.getRequestParams().keySet()) {
          replyText.append(s);
          replyText.append(" : ");
          replyText.append(userDetails.getRequestParams().get(s).getValue());
          replyText.append("\n");
        }
        replyText.append("?\nEnter next params in way paramName:value if you want to add more.\n");
        replyText.append("Or confirm search by typing 'yes'");
        reply = new Reply(replyText.toString(),
            IntentTypes.GET_ARTICLE_PARAMS);
      } else {
        reply = new Reply("Please enter params for search in form param:value",
            IntentTypes.GET_ARTICLE_PARAMS);
      }
    }
    userDetails.setReply(reply);
  }


  private Reply requestToKB(UserDetails userDetails) {

    Reply reply = new Reply();
    StringBuilder url = new StringBuilder();
    url.append(Urls.SEARCH_START.getUrl());
    Map<String, Pair<String, String>> m = userDetails.getRequestParams();

    if (m.keySet().contains("Space")) {
      String spaceCode = getSpaceCode(m.get("Space").getValue());
      System.out.println(spaceCode + " Space kode");
      if (spaceCode == null) {
        reply.setReply("There is no Space with such name in EPAM KB :(");
        return reply;
      }
    }

    String url_Body = generateUrl(userDetails.getRequestParams());
    if (url_Body == null) {
      reply.setReply("You must specify the article name in order to make search");
      return reply;
    }
    url.append(url_Body);

    url.append(Urls.SEARCH_END.getUrl());
    System.out.println("URL : " + url);

    HttpResponse<JsonNode> response;
    try {
      response = Unirest.get(url.toString())
          .header(HttpHeaders.COOKIE, userDetails.getUser().getId()).asJson();
    } catch (UnirestException e) {
      response = null;
      e.printStackTrace();
    }

    List<Result> results;
    StringBuilder replyText = new StringBuilder();
    try {
      if (response != null) {
        results = jsonParserService.parseJsonFromKbResponse(response);
        if (results != null && !results.isEmpty()) {
//                    for (Result r :
//                            results) {
          replyText.append(results.get(0).getTitle());
          replyText.append("\n");
          replyText.append("https://kb.epam.com");
          replyText.append(results.get(0).getUrl());
          //          }
        } else {
          replyText.append("Sorry, but I cant find anything on this params");
        }
      } else {
        replyText.append("Some error has happen during search\nContact Support team");
      }
    } catch (IOException e) {
      replyText.append("Sorry, some error has happen while parsing response\nContact support team");
      e.printStackTrace();
    }
    reply.setReply(replyText.toString());

    return reply;
  }


  private String getSpaceCode(String userInput) {
    List<Space> spaces = null;

    //userInput = userInput.replaceAll("\\s+","");
    try {
      spaces = spaceService.getSpacesFromKb();
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println(spaces);
    StringBuilder input = new StringBuilder();
    // input.append("'");
    input.append(userInput);
    // input.append("'");
    System.err.println(input + " USER INPUT");
    String spaceKod = null;

    for (Space s : spaces) {
      if (s.getName().equalsIgnoreCase(input.toString())) {
        spaceKod = s.getKey();
      }
    }
    System.out.println(spaceKod);
    return spaceKod;

  }


  private String generateUrl(Map<String, Pair<String, String>> params) {

    StringBuilder builder = new StringBuilder();

    Set<String> keys = params.keySet();

    if (keys.contains("Post")) {
      builder.append(params.get("Post").getKey());
//            builder.append("\"");
      builder.append(params.get("Post").getValue().replaceAll("\\s", "+"));
//            builder.append("\"");
    } else {
      return null;
    }
    if (keys.contains("Space")) {
      builder.append(params.get("Space").getKey());
      //  builder.append("\"");
      String code = getSpaceCode(params.get("Space").getValue());
      builder.append(code.replaceAll("\\s", "+"));
      //  builder.append("\"");
    }
    if (keys.contains("Author")) {
      builder.append(params.get("Author").getKey());
      // builder.append("\"");
      builder.append(params.get("Author").getValue().replaceAll("\\s", "_"));
      // builder.append("\"");
    }

    return builder.toString();
  }

}
