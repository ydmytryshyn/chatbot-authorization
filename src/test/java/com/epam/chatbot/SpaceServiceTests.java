package com.epam.chatbot;

import com.epam.chatbot.security.model.Space;
import com.epam.chatbot.security.service.SpaceService;
import java.io.IOException;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


public class SpaceServiceTests {
  private static SpaceService spaceService;

  @BeforeClass
  public static void init() {

     spaceService = new SpaceService();
  }

  @AfterClass
  public static void destroy() {
    spaceService = null;
  }

  @Test
  public void spaceListShouldBeNotNull() throws IOException {
    List<Space>spaces = spaceService.getSpacesFromKb();
    Assert.assertNotNull(spaces);
  }

}
