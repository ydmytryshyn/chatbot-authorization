package com.epam.chatbot;


import static org.assertj.core.api.Assertions.assertThat;

import com.epam.chatbot.security.MainApp;

import com.epam.chatbot.security.service.BotServiceImpl;
import com.epam.chatbot.security.service.KbAuthenticationService;
import com.epam.chatbot.security.service.ProfileInfoService;
import com.epam.chatbot.security.service.ReplyService;
import com.epam.chatbot.security.service.TextProcessorImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {MainApp.class})
public class ServicesTests {

  @Autowired
  BotServiceImpl botService;
  @Autowired
  ProfileInfoService profileInfoService;
  @Autowired
  ReplyService replyService;
  @Autowired
  TextProcessorImpl textProcessor;
  @Autowired
  KbAuthenticationService kbAuthenticationService;
  @Autowired
  AuthenticationProvider authenticationProvider;

  @Test
  public void contextLoads() throws Exception {
    assertThat(botService).isNotNull();
    assertThat(profileInfoService).isNotNull();
    assertThat(replyService).isNotNull();
    assertThat(textProcessor).isNotNull();
    assertThat(kbAuthenticationService).isNotNull();
    assertThat(authenticationProvider).isNotNull();
  }
}
