package com.epam.chatbot;


import com.epam.chatbot.security.MainApp;
import com.epam.chatbot.security.controller.AuthenticationController;
import com.epam.chatbot.security.controller.TelegramController;
import com.epam.chatbot.security.controller.TestController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {MainApp.class})
public class ControllerTests {

  @Autowired
  AuthenticationController authenticationController;
  @Autowired
  TestController testController;
  @Autowired
  TelegramController telegramController;

  @Test
  public void contextLoads() throws Exception {

    Assert.assertNotNull(authenticationController);
    Assert.assertNotNull(testController);
    Assert.assertNotNull(telegramController);
  }

}
